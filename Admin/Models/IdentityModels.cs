﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;

namespace Admin.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
		public string Firstname { get; set; }
		public string Lastname { get; set; }
		public string Middlename { get; set; }
		public string Address { get; set; }
		public string CoordinateXY { get; set; }
		public string Contactno { get; set; }
		public DateTime? DateofBirth { get; set; }
		public string PlaceofBirth { get; set; }
		public string Age { get; set; }
		public string Gender { get; set; }
		public string CivilStatus { get; set; }
		public DateTime? DateAdded { get; set; }
		public string LastModifiedBy { get; set; }
		public string UserType { get; set; }
		public bool Active { get; set; }
	}

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }


        public DbSet<QuestionaireTable> QuestionaireTables { get; set; }
        public DbSet<Subject> Subjects { get; set; }
        public DbSet<TestType> TestTypes { get; set; }
        public DbSet<multipleChoiceTable> multipleChoiceTables { get; set; }
        public DbSet<EnumerationTable> EnumerationTables { get; set; }
    }
}