﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Admin.Models
{
    public class QuestionaireTable
    {
        public int id { get; set; }

        public DateTime DateCreated { get; set; }

        public Grading grading { get; set; }

        public string CreatorsId { get; set; }

        public string numberOfItems { get; set; }

        public GradeLevel gradeLevel { get; set; }

        [ForeignKey("subject")]
        [Column(Order =1)]
        public int SubjectId { get; set; }
        public virtual Subject subject { get; set; }

        [ForeignKey("testType")]
        [Column(Order = 2)] 
        public int TestTypeId { get; set; }
        public virtual TestType testType { get; set; }
    }

    public enum GradeLevel
    {
        Grade1, Grade2, Grade3, Grade4, Grade5, Grade6, Grade7, Grade8, Grade9, Grade10, Grade11, Grade12
    }

    public enum Grading
    {
        First,
        Second,
        Third,
        Fourth
    }
}