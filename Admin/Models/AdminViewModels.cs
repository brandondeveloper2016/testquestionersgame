﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Web.Mvc;

namespace Admin.Models
{
    #region //Users
    public class UserAccessModel
    {
        public string UserType { get; set; }
        [Required]
        [Display(Name = "Fist Name")]
        public string Firstname { get; set; }

        public string Middlename { get; set; }

        [Required]
        [Display(Name = "Last Name")]
        public string Lastname { get; set; }
        [Key]
        public string UserName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        [Display(Name = "Lockout End Date Utc")]
        public DateTime? LockoutEndDateUtc { get; set; }
        public int AccessFailedCount { get; set; }
        public string PhoneNumber { get; set; }
        public IEnumerable<UserRolesModel> Roles { get; set; }
        public string Address { get; set; }
        public DateTime? DateofBirth { get; set; }
        public string PlaceofBirth { get; set; }
        public string Age { get; set; }
        public string Gender { get; set; }
        public string CivilStatus { get; set; }
        public bool Active { get; set; }
        public DateTime? DateAdded { get; set; }
        public string LastModifiedBy { get; set; }
    }

    public class UserRolesModel
    {
        [Key]
        [Display(Name = "Role Name")]
        public string RoleName { get; set; }
    }

    public class UserRoleModel
    {
        [Key]
        [Display(Name = "User Name")]
        public string UserName { get; set; }
        [Display(Name = "Role Name")]
        public string RoleName { get; set; }
    }

    public class RoleModel
    {
        [Key]
        public string Id { get; set; }
        [Display(Name = "Role Name")]
        public string RoleName { get; set; }
    }

    public class UserAndRolesModel
    {
        [Key]
        [Display(Name = "User Name")]
        public string UserName { get; set; }
        public List<UserRoleModel> colUserRoleMod { get; set; }
    }
    #endregion

    //public class DefaultConnections : DbContext
    //{
    //    public DbSet<QuestionaireTable> QuestionaireTables { get; set; }
    //    public DbSet<Subject> Subjects { get; set; }
    //    public DbSet<TestType> TestTypes { get; set; }
    //    public DbSet<multipleChoiceTable> multipleChoiceTables { get; set; }
    //    public DbSet<EnumerationTable> EnumerationTables { get; set; } 
    //}
} 