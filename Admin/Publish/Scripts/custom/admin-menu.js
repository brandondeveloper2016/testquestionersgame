﻿if (Boolean(sessionStorage.getItem("sidebar-toggle-collapsed"))) {
    $("body").removeClass('sidebar-collapse');
    sessionStorage.setItem("sidebar-toggle-collapsed", "1");
}else{
    $("body").addClass('sidebar-collapse');
    sessionStorage.setItem("sidebar-toggle-collapsed", "");
}

$('.sidebar-toggle').click(function() {
    event.preventDefault();
    if (Boolean(sessionStorage.getItem("sidebar-toggle-collapsed"))) {
        sessionStorage.setItem("sidebar-toggle-collapsed", "");
    }else{
        sessionStorage.setItem("sidebar-toggle-collapsed", "1");
    }
});

$('.sidebar-menu li:has(ul) a').click(function() {
    //$(this).next().toggle();
    if ($(this).next().is(':visible')) {
        sessionStorage.setItem($(this).text(), "exp");
    }

    if ($(this).next().is(':hidden')) {
        sessionStorage.setItem($(this).text(), "col");
    }
});

$('.sidebar-menu > li').each(function() {
    var verticalNav = sessionStorage.getItem( $(this).children('a').text() );
    if (verticalNav == 'col') {
        $(this).find('ul').show();
        //$(this).find('ul').next().toggle();
    }
});