﻿using System;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.AspNet.Identity.EntityFramework;
using Admin.Models;
using System.Collections.Generic;

namespace Admin.Controllers
{
	[Authorize(Roles = "Administrator, Agent")]
	public class AccountController : Controller
	{
		private ApplicationSignInManager _signInManager;
		private ApplicationUserManager _userManager;

		public AccountController()
		{
		}

		public AccountController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
		{
			UserManager = userManager;
			SignInManager = signInManager;
		}

		public ApplicationSignInManager SignInManager
		{
			get
			{
				return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
			}
			private set
			{
				_signInManager = value;
			}
		}

		public ApplicationUserManager UserManager
		{
			get
			{
				return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
			}
			private set
			{
				_userManager = value;
			}
		}

		//
		// GET: /Account/Login
		[AllowAnonymous]
		public ActionResult Login(string returnUrl)
		{
			ViewBag.ReturnUrl = returnUrl;
			return View();
		}

		//
		// POST: /Account/Login
		[HttpPost]
		[AllowAnonymous]
		[ValidateAntiForgeryToken]
		public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
		{
			if (!ModelState.IsValid)
			{
				return View(model);
			}

			#region //check the given username if exist as a user
			var userName = UserManager.FindByName(model.Email);
			if (userName != null)
			{
				if (userName.Active != true)
				{
					ViewBag.errorMessage = "You must have an active account to log in. "
										 + "Please ask assistance to your system admin to activate your account.";

					return View("Error");
				}
				else
				{
					var myUser = SignInManager.UserManager.Users.Where(u => u.UserName == model.Email).FirstOrDefault(); //find username for login
					var result = await SignInManager.PasswordSignInAsync(myUser.UserName, model.Password, model.RememberMe, shouldLockout: false);
					switch (result)
					{
						case SignInStatus.Success:
							return RedirectToLocal(returnUrl);
						case SignInStatus.LockedOut:
							return View("Lockout");
						case SignInStatus.RequiresVerification:
							return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, RememberMe = model.RememberMe });
						case SignInStatus.Failure:
						default:
							ModelState.AddModelError("", "Invalid login attempt.");
							return View(model);
					}
				}
			}
			#endregion

			#region //check the given email if exist as a user
			var userEmail = UserManager.FindByEmail(model.Email);
			if (userEmail != null)
			{
				if (userEmail.Active != true)
				{
					ViewBag.errorMessage = "You must have an active account to log in. "
										 + "Please ask assistance to your system admin to activate your account.";

					return View("Error");
				}
				else
				{
					var myUser = SignInManager.UserManager.Users.Where(u => u.Email == model.Email).FirstOrDefault(); //find email for login
					var result = await SignInManager.PasswordSignInAsync(myUser.UserName, model.Password, model.RememberMe, shouldLockout: false);
					switch (result)
					{
						case SignInStatus.Success:
							return RedirectToLocal(returnUrl);
						case SignInStatus.LockedOut:
							return View("Lockout");
						case SignInStatus.RequiresVerification:
							return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, RememberMe = model.RememberMe });
						case SignInStatus.Failure:
						default:
							ModelState.AddModelError("", "Invalid login attempt.");
							return View(model);
					}
				}
			}
			#endregion

			//if none is executed above then there is an error
			ModelState.AddModelError("", "User do not exist!");
			return View(model);
		}

		//
		// GET: /Account/VerifyCode
		//[AllowAnonymous]
		public async Task<ActionResult> VerifyCode(string provider, string returnUrl, bool rememberMe)
		{
			// Require that the user has already logged in via username/password or external login
			if (!await SignInManager.HasBeenVerifiedAsync())
			{
				return View("Error");
			}
			return View(new VerifyCodeViewModel { Provider = provider, ReturnUrl = returnUrl, RememberMe = rememberMe });
		}

		//
		// POST: /Account/VerifyCode
		[HttpPost]
		//[AllowAnonymous]
		[ValidateAntiForgeryToken]
		public async Task<ActionResult> VerifyCode(VerifyCodeViewModel model)
		{
			if (!ModelState.IsValid)
			{
				return View(model);
			}

			// The following code protects for brute force attacks against the two factor codes. 
			// If a user enters incorrect codes for a specified amount of time then the user account 
			// will be locked out for a specified amount of time. 
			// You can configure the account lockout settings in IdentityConfig
			var result = await SignInManager.TwoFactorSignInAsync(model.Provider, model.Code, isPersistent: model.RememberMe, rememberBrowser: model.RememberBrowser);
			switch (result)
			{
				case SignInStatus.Success:
					return RedirectToLocal(model.ReturnUrl);
				case SignInStatus.LockedOut:
					return View("Lockout");
				case SignInStatus.Failure:
				default:
					ModelState.AddModelError("", "Invalid code.");
					return View(model);
			}
		}

		//
		// GET: /Account/Register
		//[AllowAnonymous]
		public ActionResult Register()
		{
			List<string> _roles = new List<string>();
			try
			{
				using (var context = new ApplicationDbContext())
				{
					var roleStore = new RoleStore<IdentityRole>(context);
					var roleManager = new RoleManager<IdentityRole>(roleStore);
					var roles = roleManager.Roles.ToList();
					foreach (var item in roles)
					{
						_roles.Add(item.Name);
					}
				}

			}
			catch {
				_roles.Add("Temp");
			}

			ViewBag._roles = new SelectList(_roles);
			ViewBag._usrtype = "";
			return View();
		}

		//
		// POST: /Account/Register
		[HttpPost]
		//[AllowAnonymous]
		[ValidateAntiForgeryToken]
		public async Task<ActionResult> Register(RegisterViewModel model, string type)
		{
			if (ModelState.IsValid)
			{
				model.DateofBirth = DateTime.Now;
				model.DateAdded = DateTime.Now;
				model.LastModifiedBy = "";
				var user = new ApplicationUser { UserName = model.DisplayName, Firstname = model.Firstname, Middlename = model.Middlename, Lastname = model.Lastname, Email = model.Email, Active = model.Active, DateAdded = model.DateAdded, UserType = model.UserType, Address = model.Address, DateofBirth = model.DateofBirth, Contactno = model.ContactNo, CivilStatus = model.CivilStatus, Gender = model.Gender, CoordinateXY = model.CoordinateXY };
				var result = await UserManager.CreateAsync(user, model.Password);
				if (result.Succeeded)
				{
					#region Set user type
					//set default role
					try
					{
						string _usrTyp;
						if (User.IsInRole("Administrator") || User.IsInRole("Agent"))
						{
							if (model.UserType != null)
							{
								_usrTyp = model.UserType;
							}
							else
							{
								_usrTyp = "Student";
							}
						}
						else
						{
							_usrTyp = "Student";
						}

						using (var context = new ApplicationDbContext())
						{
							var roleStore = new RoleStore<IdentityRole>(context);
							var roleManager = new RoleManager<IdentityRole>(roleStore);
							roleManager.Create(new IdentityRole(_usrTyp));
							var userStore = new UserStore<ApplicationUser>(context);
							var userManager = new UserManager<ApplicationUser>(userStore);
							var user_ = userManager.FindByName(model.Email); //this is the username we registered
							if (!userManager.IsInRole(user.Id, _usrTyp))
							{
								//assign the role to this new user
								userManager.AddToRole(user.Id, _usrTyp);
							}
							context.SaveChanges();
						}
					}
					catch
					{
						ModelState.AddModelError(string.Empty, "An Error occured! please contact the Administrator to set your account type.");
						return View();
					}
					#endregion

					if (model.UserType.ToLower() == "employee")
					{
						//get user
						string getuserid = UserManager.Users.Where(u => u.Email == model.Email).Select(u => u.Id).FirstOrDefault();
						return Redirect("/EmployeeHire/Create?paramid=" + getuserid + "&name=" + model.Lastname + ", " + model.Firstname + " " + model.Middlename);
					}
					if (model.UserType.ToLower() == "student")
					{
						//get user
						string getuserid = UserManager.Users.Where(u => u.Email == model.Email).Select(u => u.Id).FirstOrDefault();
						return Redirect("/StudentEnroll/Create?paramid=" + getuserid + "&name=" + model.Lastname + ", " + model.Firstname + " " + model.Middlename);
					}
					else
					{
						return View("Success");
					}
					
				}

				//AddErrors(result);

				var strError = "";
				var txtError = result.Errors.ToList();
				foreach (var item in txtError)
				{
					strError += item + "\n";
				}
				ModelState.AddModelError("", strError);
			}


			// If we got this far, something failed, redisplay form
			List<string> _roles = new List<string>();
			using (var context = new ApplicationDbContext())
			{
				var roleStore = new RoleStore<IdentityRole>(context);
				var roleManager = new RoleManager<IdentityRole>(roleStore);
				var roles = roleManager.Roles.ToList();
				foreach (var item in roles)
				{
					_roles.Add(item.Name);
				}
			}
			ViewBag._roles = new SelectList(_roles);

			ViewBag._usrtype = model.UserType;
			return View(model);
		}



		//
		// GET: /Account/ConfirmEmail
		//[AllowAnonymous]
		public async Task<ActionResult> ConfirmEmail(string userId, string code)
		{
			if (userId == null || code == null)
			{
				return View("Error");
			}
			var result = await UserManager.ConfirmEmailAsync(userId, code);
			return View(result.Succeeded ? "ConfirmEmail" : "Error");
		}

		//
		// GET: /Account/ForgotPassword
		[AllowAnonymous]
		public ActionResult ForgotPassword()
		{
			return View();
		}

		//
		// POST: /Account/ForgotPassword
		[HttpPost]
		[AllowAnonymous]
		[ValidateAntiForgeryToken]
		public async Task<ActionResult> ForgotPassword(ForgotPasswordViewModel model)
		{
			if (ModelState.IsValid)
			{
				var user = await UserManager.FindByNameAsync(model.Email);
				if (user == null || !(await UserManager.IsEmailConfirmedAsync(user.Id)))
				{
					// Don't reveal that the user does not exist or is not confirmed
					return View("ForgotPasswordConfirmation");
				}

				// For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=320771
				// Send an email with this link
				// string code = await UserManager.GeneratePasswordResetTokenAsync(user.Id);
				// var callbackUrl = Url.Action("ResetPassword", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);		
				// await UserManager.SendEmailAsync(user.Id, "Reset Password", "Please reset your password by clicking <a href=\"" + callbackUrl + "\">here</a>");
				// return RedirectToAction("ForgotPasswordConfirmation", "Account");
			}

			// If we got this far, something failed, redisplay form
			return View(model);
		}

		//
		// GET: /Account/ForgotPasswordConfirmation
		[AllowAnonymous]
		public ActionResult ForgotPasswordConfirmation()
		{
			return View();
		}

		//
		// GET: /Account/ResetPassword
		[AllowAnonymous]
		public ActionResult ResetPassword(string code)
		{
			return code == null ? View("Error") : View();
		}

		//
		// POST: /Account/ResetPassword
		[HttpPost]
		[AllowAnonymous]
		[ValidateAntiForgeryToken]
		public async Task<ActionResult> ResetPassword(ResetPasswordViewModel model)
		{
			if (!ModelState.IsValid)
			{
				return View(model);
			}
			var user = await UserManager.FindByNameAsync(model.Email);
			if (user == null)
			{
				// Don't reveal that the user does not exist
				return RedirectToAction("ResetPasswordConfirmation", "Account");
			}
			var result = await UserManager.ResetPasswordAsync(user.Id, model.Code, model.Password);
			if (result.Succeeded)
			{
				return RedirectToAction("ResetPasswordConfirmation", "Account");
			}
			AddErrors(result);
			return View();
		}

		//
		// GET: /Account/ResetPasswordConfirmation
		[AllowAnonymous]
		public ActionResult ResetPasswordConfirmation()
		{
			return View();
		}

		//
		// POST: /Account/ExternalLogin
		[HttpPost]
		[AllowAnonymous]
		[ValidateAntiForgeryToken]
		public ActionResult ExternalLogin(string provider, string returnUrl)
		{
			// Request a redirect to the external login provider
			return new ChallengeResult(provider, Url.Action("ExternalLoginCallback", "Account", new { ReturnUrl = returnUrl }));
		}

		//
		// GET: /Account/SendCode
		[AllowAnonymous]
		public async Task<ActionResult> SendCode(string returnUrl, bool rememberMe)
		{
			var userId = await SignInManager.GetVerifiedUserIdAsync();
			if (userId == null)
			{
				return View("Error");
			}
			var userFactors = await UserManager.GetValidTwoFactorProvidersAsync(userId);
			var factorOptions = userFactors.Select(purpose => new SelectListItem { Text = purpose, Value = purpose }).ToList();
			return View(new SendCodeViewModel { Providers = factorOptions, ReturnUrl = returnUrl, RememberMe = rememberMe });
		}

		//
		// POST: /Account/SendCode
		[HttpPost]
		[AllowAnonymous]
		[ValidateAntiForgeryToken]
		public async Task<ActionResult> SendCode(SendCodeViewModel model)
		{
			if (!ModelState.IsValid)
			{
				return View();
			}

			// Generate the token and send it
			if (!await SignInManager.SendTwoFactorCodeAsync(model.SelectedProvider))
			{
				return View("Error");
			}
			return RedirectToAction("VerifyCode", new { Provider = model.SelectedProvider, ReturnUrl = model.ReturnUrl, RememberMe = model.RememberMe });
		}

		//
		// GET: /Account/ExternalLoginCallback
		[AllowAnonymous]
		public async Task<ActionResult> ExternalLoginCallback(string returnUrl)
		{
			var loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync();
			if (loginInfo == null)
			{
				return RedirectToAction("Login");
			}

			// Sign in the user with this external login provider if the user already has a login
			var result = await SignInManager.ExternalSignInAsync(loginInfo, isPersistent: false);
			switch (result)
			{
				case SignInStatus.Success:
					return RedirectToLocal(returnUrl);
				case SignInStatus.LockedOut:
					return View("Lockout");
				case SignInStatus.RequiresVerification:
					return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, RememberMe = false });
				case SignInStatus.Failure:
				default:
					// If the user does not have an account, then prompt the user to create an account
					ViewBag.ReturnUrl = returnUrl;
					ViewBag.LoginProvider = loginInfo.Login.LoginProvider;
					return View("ExternalLoginConfirmation", new ExternalLoginConfirmationViewModel { Email = loginInfo.Email });
			}
		}

		//
		// POST: /Account/ExternalLoginConfirmation
		[HttpPost]
		[AllowAnonymous]
		[ValidateAntiForgeryToken]
		public async Task<ActionResult> ExternalLoginConfirmation(ExternalLoginConfirmationViewModel model, string returnUrl)
		{
			if (User.Identity.IsAuthenticated)
			{
				return RedirectToAction("Index", "Manage");
			}

			if (ModelState.IsValid)
			{
				// Get the information about the user from the external login provider
				var info = await AuthenticationManager.GetExternalLoginInfoAsync();
				if (info == null)
				{
					return View("ExternalLoginFailure");
				}
				var user = new ApplicationUser { UserName = model.Email, Email = model.Email };
				var result = await UserManager.CreateAsync(user);
				if (result.Succeeded)
				{
					result = await UserManager.AddLoginAsync(user.Id, info.Login);
					if (result.Succeeded)
					{
						await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
						return RedirectToLocal(returnUrl);
					}
				}
				AddErrors(result);
			}

			ViewBag.ReturnUrl = returnUrl;
			return View(model);
		}

		//
		// POST: /Account/LogOff
		[AllowAnonymous]
		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult LogOff()
		{
			AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
			return RedirectToAction("Login", "Account");
		}

		//
		// GET: /Account/ExternalLoginFailure
		[AllowAnonymous]
		public ActionResult ExternalLoginFailure()
		{
			return View();
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (_userManager != null)
				{
					_userManager.Dispose();
					_userManager = null;
				}

				if (_signInManager != null)
				{
					_signInManager.Dispose();
					_signInManager = null;
				}
			}

			base.Dispose(disposing);
		}

		#region Helpers
		// Used for XSRF protection when adding external logins
		private const string XsrfKey = "XsrfId";

		private IAuthenticationManager AuthenticationManager
		{
			get
			{
				return HttpContext.GetOwinContext().Authentication;
			}
		}

		private void AddErrors(IdentityResult result)
		{
			foreach (var error in result.Errors)
			{
				ModelState.AddModelError("", error);
			}
		}

		private ActionResult RedirectToLocal(string returnUrl)
		{
			if (Url.IsLocalUrl(returnUrl))
			{
				return Redirect(returnUrl);
			}
			return RedirectToAction("Index", "Home");
		}

		internal class ChallengeResult : HttpUnauthorizedResult
		{
			public ChallengeResult(string provider, string redirectUri)
				: this(provider, redirectUri, null)
			{
			}

			public ChallengeResult(string provider, string redirectUri, string userId)
			{
				LoginProvider = provider;
				RedirectUri = redirectUri;
				UserId = userId;
			}

			public string LoginProvider { get; set; }
			public string RedirectUri { get; set; }
			public string UserId { get; set; }

			public override void ExecuteResult(ControllerContext context)
			{
				var properties = new AuthenticationProperties { RedirectUri = RedirectUri };
				if (UserId != null)
				{
					properties.Dictionary[XsrfKey] = UserId;
				}
				context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
			}
		}
		#endregion

		// Utility
		// Add RoleManager

		#region ApplicationRoleManager RoleManager
		private ApplicationRoleManager _roleManager;
		public ApplicationRoleManager RoleManager
		{
			get
			{
				return _roleManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationRoleManager>();
			}
			private set
			{
				_roleManager = value;
			}
		}
		#endregion

	}
}