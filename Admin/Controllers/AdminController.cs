﻿using Admin.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using PagedList;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace Admin.Controllers
{
	public class AdminController : Controller
	{
		private ApplicationUserManager _userManager;
		private ApplicationRoleManager _roleManager;
		ApplicationDbContext context = new ApplicationDbContext();

		//USER ACCOUNTS:==========================================================

		//GET: Admin only
		[Authorize(Roles = "Administrator")]
		public ActionResult AdminList(string search, string currentFilter, int? page)
		{
			try
			{
				int intPage = 1;
				int intPageSize = 5;
				int intTotalPageCount = 0;

				if (search != null)
				{
					intPage = 1;
				}
				else
				{
					if (currentFilter != null)
					{
						search = currentFilter;
						intPage = page ?? 1;
					}
					else
					{
						search = "";
						intPage = page ?? 1;
					}
				}
				ViewBag.CurrentFilter = search;

				//get the role id of administrator
				var role = context.Roles.SingleOrDefault(r => r.Name == "Administrator");

				//get all users with admin role id
				//var usersInRole = context.Users.Where(m => m.Roles.Any(r => r.RoleId == role.Id));
				//return View(usersInRole);

				List<UserAccessModel> col_UserMod = new List<UserAccessModel>();
				int intSkip = (intPage - 1) * intPageSize;

				intTotalPageCount = context.Users.Where(m => m.Roles.Any(r => r.RoleId == role.Id)).Count();

				var result = context.Users
						.Where(v => v.UserName.Contains(search) && v.Roles.Any(r => r.RoleId == role.Id))
						.OrderBy(v => v.UserName)
						.Skip(intSkip)
						.Take(intPageSize)
						.ToList();

				foreach (var item in result)
				{
					UserAccessModel adminlist = new UserAccessModel();
					adminlist.UserName = item.UserName;
					adminlist.Firstname = item.Firstname;
					adminlist.Middlename = item.Middlename;
					adminlist.Lastname = item.Lastname;
					adminlist.Email = item.Email;
					adminlist.Active = item.Active;
					adminlist.LastModifiedBy = item.LastModifiedBy;
					adminlist.UserType = item.UserType;
					adminlist.DateAdded = item.DateAdded;
					col_UserMod.Add(adminlist);
				}

				//set the page numbers
				var _UserModAsIpageList = new StaticPagedList<UserAccessModel>(col_UserMod, intPage, intPageSize, intTotalPageCount);

				return View(_UserModAsIpageList);
			}
			catch (Exception ex)
			{
				ModelState.AddModelError(string.Empty, "Error: " + ex);
				List<UserAccessModel> col_UserMod = new List<UserAccessModel>();

				return View(col_UserMod.ToPagedList(1, 8));
			}

		}


		//GET: Agent only
		[Authorize(Roles = "Administrator, Agent")]
		public ActionResult OtherList(string search, string currentFilter, int? page)
		{
			try
			{
				int intPage = 1;
				int intPageSize = 5;
				int intTotalPageCount = 0;

				if (search != null)
				{
					intPage = 1;
				}
				else
				{
					if (currentFilter != null)
					{
						search = currentFilter;
						intPage = page ?? 1;
					}
					else
					{
						search = "";
						intPage = page ?? 1;
					}
				}
				ViewBag.CurrentFilter = search;

				//get the role id of administrator
				var role = context.Roles.SingleOrDefault(r => r.Name != "Administrator");

				//get all users with admin role id
				//var usersInRole = context.Users.Where(m => m.Roles.Any(r => r.RoleId == role.Id));
				//return View(usersInRole);

				List<UserAccessModel> col_UserMod = new List<UserAccessModel>();
				int intSkip = (intPage - 1) * intPageSize;

				intTotalPageCount = context.Users.Where(m => m.Roles.Any(r => r.RoleId == role.Id)).Count();

				var result = context.Users
						.Where(v => v.UserName.Contains(search) && v.Roles.Any(r => r.RoleId == role.Id))
						.OrderBy(v => v.UserName)
						.Skip(intSkip)
						.Take(intPageSize)
						.ToList();

				foreach (var item in result)
				{
					UserAccessModel otherlist = new UserAccessModel();
					otherlist.UserName = item.UserName;
					otherlist.Firstname = item.Firstname;
					otherlist.Middlename = item.Middlename;
					otherlist.Lastname = item.Lastname;
					otherlist.Email = item.Email;
					otherlist.Active = item.Active;
					otherlist.LastModifiedBy = item.LastModifiedBy;
					otherlist.UserType = item.UserType;
					otherlist.DateAdded = item.DateAdded;
					col_UserMod.Add(otherlist);
				}

				//set the page numbers
				var _UserModAsIpageList = new StaticPagedList<UserAccessModel>(col_UserMod, intPage, intPageSize, intTotalPageCount);

				return View(_UserModAsIpageList);
			}
			catch (Exception ex)
			{
				ModelState.AddModelError(string.Empty, "Error: " + ex);
				List<UserAccessModel> col_UserMod = new List<UserAccessModel>();

				return View(col_UserMod.ToPagedList(1, 8));
			}

		}


		// GET: All Registered user
		[Authorize(Roles = "Administrator")]
		public ActionResult AllList(string search, string currentFilter, int? page)
		{
			try
			{
				int intPage = 1;
				int intPageSize = 5;
				int intTotalPageCount = 0;

				if (search != null)
				{
					intPage = 1;
				}
				else
				{
					if (currentFilter != null)
					{
						search = currentFilter;
						intPage = page ?? 1;
					}
					else
					{
						search = "";
						intPage = page ?? 1;
					}
				}
				ViewBag.CurrentFilter = search;

				List<UserAccessModel> col_UserMod = new List<UserAccessModel>();
				int intSkip = (intPage - 1) * intPageSize;

				intTotalPageCount = UserManager.Users
					.Where(v => v.UserName.Contains(search))
					.Count();

				var result = UserManager.Users
					.Where(v => v.UserName.Contains(search))
					.OrderBy(v => v.UserName)
					.Skip(intSkip)
					.Take(intPageSize)
					.ToList();

				foreach (var item in result)
				{
					UserAccessModel objUserMod = new UserAccessModel();

					objUserMod.UserName = item.UserName;
					objUserMod.Email = item.Email;
					objUserMod.LockoutEndDateUtc = item.LockoutEndDateUtc;
					objUserMod.Firstname = item.Firstname;
					objUserMod.Middlename = item.Middlename;
					objUserMod.Lastname = item.Lastname;
					objUserMod.DateofBirth = item.DateofBirth;
					objUserMod.Address = item.Address;
					objUserMod.PhoneNumber = item.PhoneNumber;
					objUserMod.UserType = item.UserType;
					objUserMod.Active = item.Active;
					objUserMod.LastModifiedBy = item.LastModifiedBy;
					objUserMod.DateAdded = item.DateAdded;
					col_UserMod.Add(objUserMod);
				}

				//Set the number of pages
				var _UserModAsIpageList = new StaticPagedList<UserAccessModel>(col_UserMod, intPage, intPageSize, intTotalPageCount);

				return View(_UserModAsIpageList);

			}
			catch (Exception ex)
			{
				ModelState.AddModelError(string.Empty, "Error: " + ex);
				List<UserAccessModel> col_UserMod = new List<UserAccessModel>();

				return View(col_UserMod.ToPagedList(1, 8));
			}
		}


		//GET: /Admin/Edit/TestUser
		[Authorize(Roles = "Administrator, Agent")]
		public ActionResult ViewUser(string UserName)
		{
			if (UserName == null)
			{
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}

			UserAccessModel objUserAccessMod = GetUser(UserName);
			if (objUserAccessMod == null)
			{
				return HttpNotFound();
			}
			if (objUserAccessMod.UserType == "Admin")
			{
				if (User.IsInRole("Administrator"))
				{
					ViewBag.stat = objUserAccessMod.Active;
					return View(objUserAccessMod);
				}
				else
				{
					return HttpNotFound();
				}
			}
			else
			{
				ViewBag.stat = objUserAccessMod.Active;
				return View(objUserAccessMod);
			}

		}



		// POST: /Admin/Create
		[Authorize(Roles = "Administrator")]
		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Create(UserAccessModel paramUserAccessMod)
		{
			try
			{
				if (paramUserAccessMod == null)
				{
					return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
				}

				var Email = paramUserAccessMod.Email.Trim();
				var UserName = paramUserAccessMod.UserName.Trim();
				var Password = paramUserAccessMod.Password.Trim();
				var Firstname = paramUserAccessMod.Firstname.Trim();
				var Middlename = paramUserAccessMod.Middlename.Trim();
				var Lastname = paramUserAccessMod.Lastname.Trim();
				var DateofBirth = paramUserAccessMod.DateofBirth;
				var Address = paramUserAccessMod.Address.Trim();
				var ContactNo = paramUserAccessMod.PhoneNumber.Trim();
				var position = paramUserAccessMod.UserType.Trim();
				var Active = paramUserAccessMod.Active;
				var DateAdded = DateTime.Now;

				if (Email == "")
				{
					throw new Exception("No Email");
				}

				if (Password == "")
				{
					throw new Exception("No Password");
				}

				//Username is lowercase of email
				UserName = Email.ToLower();

				//Create user
				var objnewAdminUser = new ApplicationUser { UserName = UserName, Email = Email, Active = Active, DateAdded = DateAdded };
				var AdminUserCreateResult = UserManager.Create(objnewAdminUser, Password);
				if (AdminUserCreateResult.Succeeded == true)
				{
					string strRole = Convert.ToString(Request.Form["Roles"]);
					if (strRole != "0")
					{
						//Put user in role
						UserManager.AddToRole(objnewAdminUser.Id, strRole);
					}

					return Redirect("~/Home");
				}
				else
				{
					ViewBag.Roles = GetAllRolesAsSelectList();
					ModelState.AddModelError(string.Empty, "Error: Failed to Create the user. Check Password requirements.");
					return View(paramUserAccessMod);
				}
			}
			catch (Exception ex)
			{
				ViewBag.Roles = GetAllRolesAsSelectList();
				ModelState.AddModelError(string.Empty, "Error: " + ex);
				return View("Create");
			}
		}

		//GET: /Admin/Edit/TestUser
		//[Authorize(Roles = "Administrator")]
		[Authorize]
		public ActionResult EditUser(string UserName)
		{
			if (UserName == null)
			{
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}

			if (User.Identity.Name != UserName)
			{
				if (!User.IsInRole("Administrator"))
				{
					return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
				}
			}

			UserAccessModel objUserAccessMod = GetUser(UserName);
			if (objUserAccessMod == null)
			{
				return HttpNotFound();
			}
			if (objUserAccessMod.UserType == "Administrator")
			{
				if (User.IsInRole("Administrator"))
				{
					ViewBag.stat = objUserAccessMod.Active;
					return View(objUserAccessMod);
				}
				else
				{
					return HttpNotFound();
				}
			}
			else
			{
				ViewBag.stat = objUserAccessMod.Active;
				return View(objUserAccessMod);
			}
		}

		//POST: /Admin/Edit/TestUser
		[Authorize(Roles = "Administrator")]
		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult EditUser(UserAccessModel paramUserAccessMod)
		{
			try
			{
				if (paramUserAccessMod == null)
				{
					return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
				}
				UserAccessModel objUserAccessMod = UpdateModUser(paramUserAccessMod);
				if (objUserAccessMod == null)
				{
					return HttpNotFound();
				}

				//check if set to ACTIVE
				if (paramUserAccessMod.Active == true)
				{
					try
					{
						if (paramUserAccessMod.UserType == "Student" || paramUserAccessMod.UserType == "Teacher")
						{
							ApplicationDbContext context = new ApplicationDbContext();

							var userStore = new UserStore<ApplicationUser>(context);
							var userManager = new UserManager<ApplicationUser>(userStore);
							var user = userManager.FindByName(objUserAccessMod.UserName);


						}
					}
					catch
					{
						ModelState.AddModelError(string.Empty, "Error: Error Connecting to Server");
						return View("EditUser", GetUser(paramUserAccessMod.UserName));
					}

				}

				//Redirect(Request.UrlReferrer.ToString());
				if (User.IsInRole("Administrator"))
				{
					return Redirect("~/admin/alllist");
				}
				else
				{
					return Redirect("~/admin/otherlist");
				}
			}
			catch(Exception ex)
			{
				var errorInfo = ex;
				//ModelState.AddModelError(string.Empty, "Error: " + ex);
				ModelState.AddModelError(string.Empty, "Error: The email provided is already in use!");
				return View("EditUser", GetUser(paramUserAccessMod.UserName));
			}
		}

		//Delete: /Admin/DeleteUser
		[Authorize(Roles = "Administrator")]
		public ActionResult DeleteUser(string _UserName)
		{
			var UserName = _UserName;
			try
			{
				if (UserName == null)
				{
					return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
				}
				if (UserName.ToLower() == this.User.Identity.Name.ToLower())
				{
					ModelState.AddModelError(string.Empty, "Error: Cannot delete the current user");
					return View("EditUser");
				}

				UserAccessModel objUserAccessMod = GetUser(UserName);
				if (objUserAccessMod == null)
				{
					return HttpNotFound();
				}
				else
				{
					DeleteUser(objUserAccessMod);
				}
				return Redirect("~/admin/alllist");
			}
			catch
			{
				//ModelState.AddModelError(string.Empty, "Error: " + ex);
				ModelState.AddModelError(string.Empty, "Error: Cannot delete the current user");
				return View("EditUser", GetUser(UserName));
			}
		}

		//GET: /Admin/EditRoles/TestUser
		[Authorize(Roles = "Administrator")]
		public ActionResult EditRoles(string UserName)
		{
			if (UserName == null)
			{
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}
			//if (User.Identity.Name != UserName)
			//{
			//	return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			//}

			UserName = UserName.ToLower();

			//check that we have an actual user
			UserAccessModel objUserAccessMod = new UserAccessModel();
			if (objUserAccessMod == null)
			{
				return HttpNotFound();
			}

			UserAndRolesModel objUserAndRolesMod = GetUserAndRoles(UserName);

			return View(objUserAndRolesMod);
		}

		//POST: /Admin/EditRoles/TestUser
		[Authorize(Roles = "Administrator")]
		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult EditRoles(UserAndRolesModel paramUserAndRolesMod)
		{
			try
			{
				if (paramUserAndRolesMod == null)
				{
					return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
				}

				string UserName = paramUserAndRolesMod.UserName;
				string strNewRole = Convert.ToString(Request.Form["AddRole"]);

				if (strNewRole != "No Roles Found")
				{
					//Get the user
					ApplicationUser user = UserManager.FindByName(UserName);

					//put user in role
					UserManager.AddToRole(user.Id, strNewRole);
				}

				ViewBag.AddRoles = new SelectList(RoleUserIsNotIn(UserName));

				UserAndRolesModel objUserAndRolesMod = GetUserAndRoles(UserName);

				return View(objUserAndRolesMod);
			}
			catch (Exception ex)
			{
				ModelState.AddModelError(string.Empty, "Error: " + ex);
				return View("EditRoles");
			}
		}

		//Delete: /Admin/DeleteRole
		[Authorize(Roles = "Administrator")]
		public ActionResult DeleteRole(string UserName, string RoleName)
		{
			try
			{
				if (UserName == null)
				{
					return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
				}
				//if (User.Identity.Name != UserName)
				//{
				//	return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
				//}

				UserName = UserName.ToLower();

				//check taht we have an actual user
				UserAccessModel objUserAccessMod = GetUser(UserName);
				if (objUserAccessMod == null)
				{
					return HttpNotFound();
				}

				if (UserName.ToLower() == this.User.Identity.Name.ToLower() && RoleName == "Admin")
				{
					ModelState.AddModelError(string.Empty, "Error: Cannot Delete Admin Role for the current user");
				}

				//Get the user
				ApplicationUser user = UserManager.FindByName(UserName);
				//Remove User from Role
				UserManager.RemoveFromRole(user.Id, RoleName);
				UserManager.Update(user);

				ViewBag.AddRole = new SelectList(RoleUserIsNotIn(UserName));

				return RedirectToAction("EditRoles", new { UserName = UserName });

			}
			catch (Exception ex)
			{
				ModelState.AddModelError(string.Empty, "Error: " + ex);
				ViewBag.AddRole = new SelectList(RoleUserIsNotIn(UserName));

				UserAndRolesModel objUserAndRolesMod = GetUserAndRoles(UserName);

				return View("EditRoles", objUserAndRolesMod);
			}
		}






		//ROLES SECTION: ==================================================================


		//GET: /Admin/ViewAllRoles
		[Authorize(Roles = "Administrator")]
		public ActionResult ViewAllRoles()
		{
			var roleManager = new RoleManager<IdentityRole>(
				new RoleStore<IdentityRole>(new ApplicationDbContext())
				);

			List<RoleModel> colRoleMod = (from objRole in roleManager.Roles
										  select new RoleModel { Id = objRole.Id, RoleName = objRole.Name }).ToList();

			return View(colRoleMod);
		}

		//GET: /Admin/AddRole
		[Authorize(Roles = "Administrator")]
		public ActionResult AddRole()
		{
			RoleModel objRoleMod = new RoleModel();

			return View(objRoleMod);
		}

		//POST: /Admin/AddRole
		[Authorize(Roles = "Administrator")]
		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult AddRole(RoleModel paramRoleMod)
		{
			try
			{
				if (paramRoleMod == null)
				{
					return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
				}

				var RoleName = paramRoleMod.RoleName.Trim();
				if (RoleName == "")
				{
					throw new Exception("No RoleName");
				}

				//Create Role
				var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(new ApplicationDbContext()));
				if (!roleManager.RoleExists(RoleName))
				{
					roleManager.Create(new IdentityRole(RoleName));
				}

				return Redirect("~/Admin/ViewAllRoles");
			}
			catch (Exception ex)
			{
				ModelState.AddModelError(string.Empty, "Error: " + ex);
				return View("AddRole");
			}
		}

		//POST: /Admin/DeleteUserRole?RoleName=TestRole
		[Authorize(Roles = "Administrator")]
		public ActionResult DeleteUserRole(string RoleName)
		{
			try
			{
				if (RoleName == null)
				{
					return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
				}

				if (RoleName.ToLower() == "admin")
				{
					throw new Exception(String.Format("Cannot delete {0} Role."));
				}

				var rolemanager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(new ApplicationDbContext()));

				var UserInRole = rolemanager.FindByName(RoleName).Users.Count();
				if (UserInRole > 0)
				{
					throw new Exception(String.Format("Cannot delete {0} Role because it still has users.", RoleName));
				}

				var objRoleToDelete = (from objRole in rolemanager.Roles
									   where objRole.Name == RoleName
									   select objRole).FirstOrDefault();
				if (objRoleToDelete != null)
				{
					rolemanager.Delete(objRoleToDelete);
				}
				else
				{
					throw new Exception(String.Format("Cannot delete {0}, Role does not exist.", RoleName));
				}

				List<RoleModel> colRoleMod = (from objRole in rolemanager.Roles
											  select new RoleModel { Id = objRole.Id, RoleName = objRole.Name }).ToList();

				return View("ViewAllRoles", colRoleMod);
			}
			catch (Exception ex)
			{
				ModelState.AddModelError(String.Empty, "Error: " + ex);

				var rolemanager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(new ApplicationDbContext()));

				List<RoleModel> colRoleMod = (from objRole in rolemanager.Roles
											  select new RoleModel { Id = objRole.Id, RoleName = objRole.Name }).ToList();

				return View("ViewAllRoles", colRoleMod);
			}
		}



		//FIRST USE ACTIVATION  ================================
		public ActionResult Activate()
		{
			try
			{
				using (var context = new ApplicationDbContext())
				{
					// Get Admin Account
					string AdminUserName = ConfigurationManager.AppSettings["AdminUserName"];
					string AdminEmail = ConfigurationManager.AppSettings["AdminEmail"];
					string AdminPassword = ConfigurationManager.AppSettings["AdminPassword"];

					var roleStore = new RoleStore<IdentityRole>(context);
					var roleManager = new RoleManager<IdentityRole>(roleStore);
					roleManager.Create(new IdentityRole("Administrator"));
					var userStore = new UserStore<ApplicationUser>(context);
					var userManager = new UserManager<ApplicationUser>(userStore);
					var user = userManager.FindByName(AdminUserName); //this is the username we registered as admin
					if (user == null)
					{
						var objnewAdminuser = new ApplicationUser { UserName = AdminUserName, Email = AdminEmail, Active = true, DateAdded = DateTime.Now, UserType = "Administrator" };
						var CreateAdmin = userManager.Create(objnewAdminuser, AdminPassword);
						//assign the role to this new user
						userManager.AddToRole(objnewAdminuser.Id, "Administrator");
					}
					else
					{
						if (user.Active != true)
						{
							user.Active = true;
							userManager.Update(user);
						}
			
						if (!userManager.IsInRole(user.Id, "Administrator"))
						{
							//assign the role to this new user
							userManager.AddToRole(user.Id, "Administrator");
						}
					}
					context.SaveChanges();
				}
			}
			catch(Exception ex)
			{
				var errorInfo = ex;
				ModelState.AddModelError(string.Empty, "Site activation Failed!");
				return RedirectToAction("Login", "Account");
			}
			return View();
		}








		//Utility  ================================

		public ApplicationUserManager UserManager
		{
			get
			{
				return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
			}
			private set
			{
				_userManager = value;
			}
		}

		public ApplicationRoleManager RoleManager
		{
			get
			{
				return _roleManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationRoleManager>();
			}
			private set
			{
				_roleManager = value;
			}
		}

		private List<SelectListItem> GetAllRolesAsSelectList()
		{
			List<SelectListItem> SelectRoleListItems = new List<SelectListItem>();

			var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(new ApplicationDbContext()));

			var colRoleSelectList = roleManager.Roles.OrderBy(v => v.Name).ToList();

			SelectRoleListItems.Add(new SelectListItem { Text = "Select", Value = "0" });

			foreach (var item in colRoleSelectList)
			{
				SelectRoleListItems.Add(new SelectListItem { Text = item.ToString(), Value = item.ToString() });
			}

			return SelectRoleListItems;
		}

		private UserAccessModel GetUser(string paramUserName)
		{
			UserAccessModel objUserAccessMod = new UserAccessModel();

			var result = UserManager.FindByName(paramUserName);

			//user not found
			if (result == null)
			{
				throw new Exception("User not found!");
			}
			objUserAccessMod.UserType = result.UserType;
			objUserAccessMod.UserName = result.UserName;
			objUserAccessMod.Email = result.Email;
			objUserAccessMod.LockoutEndDateUtc = result.LockoutEndDateUtc;
			objUserAccessMod.AccessFailedCount = result.AccessFailedCount;
			objUserAccessMod.PhoneNumber = result.PhoneNumber;
			objUserAccessMod.Firstname = result.Firstname;
			objUserAccessMod.Middlename = result.Middlename;
			objUserAccessMod.Lastname = result.Lastname;
			objUserAccessMod.DateofBirth = result.DateofBirth;
			objUserAccessMod.Address = result.Address;
			objUserAccessMod.UserType = result.UserType;
			objUserAccessMod.Active = result.Active;
			objUserAccessMod.DateAdded = result.DateAdded;
			objUserAccessMod.LastModifiedBy = result.LastModifiedBy;
			return objUserAccessMod;
		}

		private UserAccessModel UpdateModUser(UserAccessModel paramUserAccessMod)
		{
			ApplicationUser result = UserManager.FindByName(paramUserAccessMod.UserName);
			//user not found
			if (result == null)
			{
				throw new Exception("User not found!");
			}
			if (result.Email != paramUserAccessMod.Email)
			{
				ApplicationUser checkemail = UserManager.FindByEmail(paramUserAccessMod.Email);
				if (checkemail != null)
				{
					throw new Exception("Email is used by other users!");
				}
			}
			result.UserType = paramUserAccessMod.UserType;
			result.Email = paramUserAccessMod.Email;
			result.Firstname = paramUserAccessMod.Firstname;
			result.Middlename = paramUserAccessMod.Middlename;
			result.Lastname = paramUserAccessMod.Lastname;
			result.DateofBirth = paramUserAccessMod.DateofBirth;
			result.Address = paramUserAccessMod.Address;
			result.PhoneNumber = paramUserAccessMod.PhoneNumber;
			result.UserType = paramUserAccessMod.UserType;
			result.Active = paramUserAccessMod.Active;
			result.DateAdded = result.DateAdded; //no changes

			var dateToday = DateTime.Now.ToString();
			result.LastModifiedBy = User.Identity.GetUserName() + " - " + dateToday;



			//Check if account need to be unlock
			if (UserManager.IsLockedOut(result.Id))
			{
				//unlock user
				UserManager.ResetAccessFailedCountAsync(result.Id);
			}

			UserManager.Update(result);

			//was a new password sent accross?
			if (!String.IsNullOrEmpty(paramUserAccessMod.Password))
			{
				//Remove current password
				var removePassword = UserManager.RemovePassword(result.Id);
				if (removePassword.Succeeded)
				{
					//Add the new password
					var AddPassword = UserManager.AddPassword(result.Id, paramUserAccessMod.Password);

					if (AddPassword.Errors.Count() > 0)
					{
						throw new Exception(AddPassword.Errors.FirstOrDefault());
					}
				}
			}
			return paramUserAccessMod;
		}

		private void DeleteUser(UserAccessModel paramUserAccessMod)
		{
			ApplicationUser user = UserManager.FindByName(paramUserAccessMod.UserName);
			//user not found
			if (user == null)
			{
				throw new Exception("User not found!");
			}

			UserManager.RemoveFromRoles(user.Id, UserManager.GetRoles(user.Id).ToArray());
			UserManager.Update(user);
			UserManager.Delete(user);

		}

		private UserAndRolesModel GetUserAndRoles(string UserName)
		{
			//Get the user
			ApplicationUser user = UserManager.FindByName(UserName);

			List<UserRoleModel> colUserRoleMod = (from objRole in UserManager.GetRoles(user.Id)
												  select new UserRoleModel
												  {
													  RoleName = objRole,
													  UserName = UserName
												  }).ToList();

			if (colUserRoleMod.Count() == 0)
			{
				colUserRoleMod.Add(new UserRoleModel { RoleName = "No Roles Found" });
			}

			ViewBag.AddRole = new SelectList(RoleUserIsNotIn(UserName));

			//Create UserAndRolesMod
			UserAndRolesModel objUserAndRolesMod = new UserAndRolesModel();
			objUserAndRolesMod.UserName = UserName;
			objUserAndRolesMod.colUserRoleMod = colUserRoleMod;

			return objUserAndRolesMod;
		}

		private List<string> RoleUserIsNotIn(string UserName)
		{
			//Get roles of user
			var colAllRoles = RoleManager.Roles.Select(v => v.Name).ToList();

			//Get roles for an individual user
			ApplicationUser user = UserManager.FindByName(UserName);
			//user not found
			if (user == null)
			{
				throw new Exception("User not found!");
			}

			var colRolesForUser = UserManager.GetRoles(user.Id).ToList();
			var colRolesUserInNotIn = (from objRole in colAllRoles where !colRolesForUser.Contains(objRole) select objRole).ToList();

			if (colRolesUserInNotIn.Count() == 0)
			{
				colRolesUserInNotIn.Add("No Roles Found");
			}

			return colRolesUserInNotIn;
		}

	}
}