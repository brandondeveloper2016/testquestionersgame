﻿using Admin.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Services;
using System.Web.Security;

namespace Admin.Controllers
{

	public class HomeController : Controller
	{

		[Authorize]
		public ActionResult Index()
		{
			return View();
		}

		[Authorize(Roles = "Administrator, Agent")]
		public ActionResult Dashboard()
		{
			return View();
		}

		[Authorize(Roles = "Administrator, Agent")]
		public ActionResult About()
		{
			ViewBag.Message = "Your application description page.";

			return View();
		}


	}
}